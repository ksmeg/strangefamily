function judge (){
    var family = strange.family;
    var dog = [];
    var dad = [];
    var mom = [];
    var BSide = [];
// 家族の場所を配列に入れて、ルールと照らし合わせ
    for(key in family){
        if (family[key].whichSide == strange.family.dog.whichSide){
            if(key != "dog"){
                dog.push(key);
            }
        }
        if (family[key].whichSide == strange.family.dad.whichSide){
            dad.push(key);
        }
        if (family[key].whichSide == strange.family.mom.whichSide){
            mom.push(key);
        }
        if (family[key].whichSide == "B"){
            BSide.push(key);
        }
    }
// 最短手数のクリア時の表示
    if(BSide.length == 8 && strange.boat.count == 17){
        strange.game.showScreen("#result-screen");
        $("#tensai img").css("display", "block");
        $("#saitan img").css("display", "block");
        $("#te img").css("display", "block");
        $("#num-box-left .one").css("display", "block");
        $("#num-box-right .seven").css("display", "block");
        setTimeout(toTop,msec);
        $("#good-family img").click(function(){
            strange.game.showScreen("#splash-screen");
            init_family();
            init_boat();
            init_result_screen();
        })
    }

// ゲームオーバーの画面表示から、再チャレンジのテキスト表示までの時間指定
    var msec = 500;
    function showRetry(){
        $("#retry img").css("display", "block");
    }
    function toTop(){
        $("#good-family img").css("display", "block");
    }

// クリアはしたけど最短手数ではない場合
    if(BSide.length == 8 && strange.boat.count > 17){
        strange.game.showScreen("#result-screen");
        $("#shusai img").css("display", "block");
        $("#te img").css("display", "block");
        $("#ato img").css("display", "block");
        var str;
        var num = strange.boat.count;
        var num_str = num.toString();
        if (num_str.length>1){
            var left = num_str.substring(num_str.length - 2, num_str.length - 1);
            str = numToText(left);
            left = str
            $(`#num-box-left .${left}`).css("display", "block");
        }
        var right = num_str.substring(num_str.length - 1, num_str.length);
        str = numToText(right);
        right = str;
        $(`#num-box-right .${right}`).css("display", "block");
        
        function numToText(numStr){
            switch(numStr){
                case "1": return "one";
                case "2": return "two";
                case "3": return "three";
                case "4": return "four"; 
                case "5": return "five";
                case "6": return "six";
                case "7": return "seven";
                case "8": return "eight";
                case "9": return "nine";
                case "0": return  "zero";
            }
        }
        
    // クリアまでに何手かかったか、あと何手短縮できるかの計算と表示
        atoNum = num - 17;
        atoNum = atoNum.toString();
        if (atoNum.length>1){
            var atoLeft = atoNum.substring(atoNum.length - 2, atoNum.length - 1);
            str = numToText(atoLeft);
            atoLeft = str;
            $(`#num-box-left .${atoLeft}`).clone().appendTo("#ato-num").css({"height":"1.1em", "display":"block"});
            $(`#ato-num .${atoLeft}`).removeClass("num");
            var atoRight = atoNum.substring(atoNum.length - 1, atoNum.length);
            str = numToText(atoRight);
            atoRight = str;
            $(`#num-box-right .${atoRight}`).clone().appendTo("#ato-num").css({"margin-left":"-0.15em", "height":"1.1em", "display":"block"});
            $(`#ato-num .${atoRight}`).removeClass("num");
            $("#ato-num").css({"top":"15.4em", "left":"8.8em"})
        }else{
            var atoRight = atoNum.substring(atoNum.length - 1, atoNum.length);
            str = numToText(atoRight);
            atoRight = str;
            $(`#num-box-right .${atoRight}`).clone().appendTo("#ato-num").css({"margin-left":"0.3em", "height":"1.3em", "display":"block"});
            $(`#ato-num .${atoRight}`).removeClass("num");
        }
        setTimeout(showRetry,msec);
    }
// 失敗した時のテキスト表示
    if(dog.indexOf("meido") < 0 && dog.length > 0){
        strange.game.showScreen("#result-screen");
        $("#inu img").css("display", "block");
        setTimeout(showRetry,msec);
    }

    if(dad.indexOf("mom") < 0 && dad.indexOf("dau1") > 0){
        strange.game.showScreen("#result-screen");
        $("#titi img").css("display", "block");
        setTimeout(showRetry,msec);
    }
    if(dad.indexOf("mom") < 0 && dad.indexOf("dau2") > 0){
        strange.game.showScreen("#result-screen");
        $("#titi img").css("display", "block");
        setTimeout(showRetry,msec);
    }

    if(mom.indexOf("dad") < 0 && mom.indexOf("son1") > 0){
        strange.game.showScreen("#result-screen");
        $("#haha img").css("display", "block");
        setTimeout(showRetry,msec);
    }
    if(mom.indexOf("dad") < 0 && mom.indexOf("son2") > 0){
        strange.game.showScreen("#result-screen");
        $("#haha img").css("display", "block");
        setTimeout(showRetry,msec);
    }
}