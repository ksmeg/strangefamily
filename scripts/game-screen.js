// ゲームのプレイ画面の設定
strange.screens["#game-screen"] = (function() {
    var firstRun = true;
    function setup() {
        $("#launch img").click(function() {
        });
        $("#rule_check img").click(function() {
            strange.game.showScreen("#rule-screen");
        });
    }
    function run() {
        if(firstRun) {
            setup();
            firstRun = false;
        }
    }
    return {
        run : run
    };
})();