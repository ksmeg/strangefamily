// 結果画面の設定
strange.screens["#result-screen"] = (function() {
    var firstRun = true;
    function setup() {
        $("#retry img").click(function(){
            strange.game.showScreen("#game-screen");
            setTimeout (init_family, 500);
            setTimeout (init_boat, 550);
            setTimeout (init_result_screen, 600);
        })
    }
    function run() {
        if(firstRun) {
            setup();
            firstRun = false;
        }
    }
    return {
        run : run
    };
})();