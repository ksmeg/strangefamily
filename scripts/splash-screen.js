// スプラッシュ画面の設定
strange.screens["#splash-screen"] = (function() {
    var firstRun = true;
    function setup() {
        $("#tap").click(function() {
            strange.game.showScreen("#rule-screen");
        });
    }
    function run() {
        if(firstRun) {
            setup();
            firstRun = false;
        }
    }
    return {
        run : run
    };
})();
