// ルール表示画面の設定
strange.screens["#rule-screen"] = (function() {
    var firstRun = true;
    function setup() {
        $("#start img").click(function() {
            strange.game.showScreen("#game-screen");
        });
    }
    function run() {
        if(firstRun) {
            setup();
            firstRun = false;
        }
    }
    return {
        run : run
    };
})();