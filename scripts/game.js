// 画面切り替えメソッドの設定
strange.game = (function() {
    function showScreen(screenId) {
        var activeScreen = $("#game").find(".screen.active").attr("id"),
            screenId = "#" + $("#game").find(screenId).attr("id");
            console.log(screenId);
        if(activeScreen) {
            $("#" + activeScreen).removeClass("active");
        }
        strange.screens[screenId].run();
        $(screenId).addClass("active");
    }
    return {
        showScreen : showScreen
    };
})();

