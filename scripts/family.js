// 家族のプロパティとメソッドの設定
function family(who, whichSide, driver, onBaot, order, seatNum) {
    this.who = who,
    this.whichSide = whichSide,
    this.driver = driver,
    this.onBaot = onBaot,
    this.order = order,
    this.seatNum = seatNum,
    this.toASide = function(){
        var top = 3.1 * this.order + "em";
        $(`#${this.who} img`).css({
            "transition-duration": "0.5s", 
            top: top, 
            left : 0
        });
        this.whichSide = "A";
        switch(this.seatNum){
            case 1: strange.boat.seat1 = 0; break;
            case 2: strange.boat.seat2 = 0; break;
        }
        this.seatNum = 0;
        this.onBaot = "no";
        if(this.driver == true){
            strange.boat.driver -= 1;
        }
    },      
    this.toBSide = function(){
        var top = 3.1 * this.order + "em";
        $(`#${this.who} img`).css({
            "transition-duration": "0.5s", 
            top: top, 
            left : "18em"
        });
        this.whichSide = "B";
        switch(this.seatNum){
            case 1: strange.boat.seat1 = 0; break;
            case 2: strange.boat.seat2 = 0; break;
        }
        this.seatNum = 0;
        this.onBaot = "no";
        if(this.driver == true){
            strange.boat.driver -= 1;
        }
    },
    this.toBoatSeat1 = function(){
        var fontSize = parseInt($("#game").css("fontSize").replace( /px/g , ""));
        var x = $("#boat img").position();
        $(`#${this.who} img`).css({
            "transition-duration": "0.5s",
            "top": x.top / fontSize - 2.2 + "em",
            "left": x.left / fontSize + 1.55 + "em"
        });
        this.onBaot = "yes";
        this.seatNum = 1;
        strange.boat.seat1 = this.who;
        if(this.driver == true){
            strange.boat.driver += 1;
        }
    },
    this.toBoatSeat2 = function(){
        var fontSize = parseInt($("#game").css("fontSize").replace( /px/g , ""));
        var x = $("#boat img").position();
        $(`#${this.who} img`).css({
            "transition-duration": "0.5s",
            "top": x.top / fontSize - 2.2 + "em",
            "left": x.left / fontSize - 1.15 + "em"
        });
        this.onBaot = "yes";
        this.seatNum = 2;
        strange.boat.seat2 = this.who;
        if(this.driver == true){
            strange.boat.driver += 1;
        }
    },
    this.yurayura = function () {
        // ボートに乗った時に、ボートの揺らぎに同期するメソッド
        var This = this;
        if(this.onBaot == "no"){
            $(`#${this.who} img`).removeClass("horizontal");
            return;
        }
        function yura () {
            var x = $("#boat").position().left;
            if (x > 1.8 && This.onBaot == "yes") {
                $(`#${This.who} img`).addClass("horizontal");
                clearTimeout (timer);
                return;
            }
            var timer = setTimeout(yura,10);
        }
        yura();
    }
}

// 家族のインスタンス作成
strange.family.dad = new family("dad", "A", true, "no", 0, 0);
strange.family.mom = new family("mom", "A", true, "no", 1, 0);
strange.family.son1 = new family("son1", "A", false, "no", 2, 0);
strange.family.son2 = new family("son2", "A", false, "no", 3, 0);
strange.family.dau1 = new family("dau1", "A", false, "no", 4, 0);
strange.family.dau2 = new family("dau2" ,"A", false, "no", 5, 0);
strange.family.meido = new family("meido" ,"A", true, "no", 6, 0);
strange.family.dog = new family("dog", "A", false, "no", 7, 0);

// 家族をタップした時のアクション
$(".family").click(function(){
    var id = this.id;
    switch(id){
        case "dad": var family = strange.family.dad;
        break;
        case "mom": var family = strange.family.mom;
        break;
        case "son1": var family = strange.family.son1;
        break;
        case "son2": var family = strange.family.son2;
        break;
        case "dau1": var family = strange.family.dau1;
        break;
        case "dau2": var family = strange.family.dau2;
        break;
        case "meido": var family = strange.family.meido;
        break;
        case "dog": var family = strange.family.dog;
        break;
    }

    if(family.whichSide == strange.boat.whichSide && family.onBaot == "no"){
        toBoat();
    }else if(family.whichSide != strange.boat.whichSide && family.onBaot == "no"){
        console.log("dive!!");
    }else if (family.onBaot == "yes"){
        toField();
    }

    function toBoat (){
        if (strange.boat.seat1 == 0 ){
            family.toBoatSeat1();
        }else if (strange.boat.seat2 == 0){
            family.toBoatSeat2();
        } else {
            console.log("boat is full");
        }
        family.yurayura();
    }

    function toField (){
        if (family.whichSide == "A"){
            family.toASide();
        }else if (family.whichSide == "B"){
            family.toBSide();
        }
        family.yurayura();
    }
    
});