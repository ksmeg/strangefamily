// ボートのプロパティとメソッド設定
strange.boat = {
    "whichSide" : "A",
    "seat1" : 0,
    "seat2" : 0,
    "driver" : 0,
    "toASide" : function (){
        $("#boat img").css({
            "transition-duration": "0.5s", 
            left : "4.5em"
        });
        strange.boat.count += 1;
    },
    "toBSide" : function (){
        $("#boat img").css({
            "transition-duration": "0.5s", 
            left : "14.5em"
        });
        strange.boat.count += 1; 
    },
    "count" : 0
}

// 出発ボタンをタップ時のアクション
$("#launch").click(function(){
    $("#launch").css("pointer-events", "none");
    if (strange.boat.driver > 0){
        launchBoat();
    }else{
        console.log("ボートが流された");
        $("#launch").css("pointer-events", "auto");
    }
});

// ボートの移動と、乗船者の移動
function launchBoat (){
    var passenger = [];
    if(strange.boat.whichSide == "A"){
        strange.boat.toBSide();
        if(strange.boat.seat1 != 0){
            $(`#${strange.boat.seat1} img`).css({
                "transition-duration": "0.5s",  
                left : "15.7em"
            });
            var family = getFamilyObject(strange.boat.seat1);
            family.whichSide = "B";
            passenger.push(family);
        }
        if(strange.boat.seat2 != 0){
            $(`#${strange.boat.seat2} img`).css({
                "transition-duration": "0.5s",  
                left : "13.3em"
            });
            var family = getFamilyObject(strange.boat.seat2);
            family.whichSide = "B";
            passenger.push(family);
        }
        strange.boat.whichSide = "B";
        setTimeout(boatArrive, 300);

    }else if(strange.boat.whichSide == "B"){
        strange.boat.toASide();
        if(strange.boat.seat1 != 0){
            $(`#${strange.boat.seat1} img`).css({
                "transition-duration": "0.5s",  
                left : "5.7em"
            });
            var family = getFamilyObject(strange.boat.seat1);
            family.whichSide = "A";
            passenger.push(family);
        }
        if(strange.boat.seat2 != 0){
            $(`#${strange.boat.seat2} img`).css({
                "transition-duration": "0.5s",  
                left : "3.3em"
            });
            var family = getFamilyObject(strange.boat.seat2);
            family.whichSide = "A";
            passenger.push(family);
        }
        strange.boat.whichSide = "A";
        setTimeout(boatArrive, 300);
    }

    function boatArrive (){
        for (i=0;i<passenger.length;i++){
            var family = passenger[i];
            toField(family);
        }
        $("#launch").css("pointer-events", "auto");
        setTimeout(judge, 600);
    }

    function getFamilyObject(string){
        switch(string){
            case "dad": var family = strange.family.dad;
            return family;
            case "mom": var family = strange.family.mom;
            return family;
            case "son1": var family = strange.family.son1;
            return family;
            case "son2": var family = strange.family.son2;
            return family;
            case "dau1": var family = strange.family.dau1;
            return family;
            case "dau2": var family = strange.family.dau2;
            return family;
            case "meido": var family = strange.family.meido;
            return family;
            case "dog": var family = strange.family.dog;
            return family;
        }
    }

    // 乗船者がボートからフィールドに移動する時の条件
    function toField (family){
        if (family.whichSide == "A"){
            family.toASide();
        }else if (family.whichSide == "B"){
            family.toBSide();
        }
        family.yurayura();
    }
}