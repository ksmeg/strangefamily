/*ゲームの初期化*/
function init_family (){
    strange.family.dad = new family("dad", "A", true, "no", 0, 0);
    strange.family.mom = new family("mom", "A", true, "no", 1, 0);
    strange.family.son1 = new family("son1", "A", false, "no", 2, 0);
    strange.family.son2 = new family("son2", "A", false, "no", 3, 0);
    strange.family.dau1 = new family("dau1", "A", false, "no", 4, 0);
    strange.family.dau2 = new family("dau2" ,"A", false, "no", 5, 0);
    strange.family.meido = new family("meido" ,"A", true, "no", 6, 0);
    strange.family.dog = new family("dog", "A", false, "no", 7, 0);
    strange.family.dad.toASide();
    strange.family.mom.toASide();
    strange.family.son1.toASide();
    strange.family.son2.toASide();
    strange.family.dau1.toASide();
    strange.family.dau2.toASide();
    strange.family.meido.toASide();
    strange.family.dog.toASide();
    strange.boat.toASide();
}

function init_boat (){
    strange.boat.whichSide = "A";
    strange.boat.seat1 = 0;
    strange.boat.seat2 = 0;
    strange.boat.driver = 0;
    strange.boat.count = 0;
}

function init_result_screen (){
    $("#tensai img").css("display", "none");
    $("#saitan img").css("display", "none");
    $("#shusai img").css("display", "none");
    $("#te img").css("display", "none");
    $("#ato img").css("display", "none");
    $("#inu img").css("display", "none");
    $("#titi img").css("display", "none");
    $("#haha img").css("display", "none");
    $("#retry img").css("display", "none");
    $("#good-family img").css("display", "none");
    init_number();
}

function init_number (){
    number = ["one","two","three","four","five","six","seven","eight","nine","zero"];
    for(i=0;i<number.length;i++){
        $(`#num-box-left .${number[i]}`).css("display", "none");
        $(`#num-box-right .${number[i]}`).css("display", "none");
        $(`#ato-num .${number[i]}`).css("display", "none");
    }
}